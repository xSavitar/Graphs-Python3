#!/usr/bin/python3

# Author: Alangi Derick (d3r1ck)
# Email: alangiderick@gmail.com

# Import modules
from queue import Queue

# Define the BFS Algorithm
def bfs(graph, s_vertex=None, e_vertex=None):
	"""
	Perform BFS traversal of a graph originating from s_vertex 
	and ends at e_vertex. Builds and returns a dictionary with 
	vertices mapped to their immediate predecessors.
	"""

	predecessor = {}

	if graph:
		if not s_vertex:
			s_vertex = list(graph.keys())[0]

	# Creating a Queue object
	q = Queue()
	q.put(s_vertex)

	# Search will the queue holds vertices yet to visit.
	while not q.empty() and e_vertex not in predecessor:
		vertex = q.get() # get vertex on the front of the queue
		for adjacent_vertex in graph[vertex]: # Consider all adjacent vertices
		# Has the predecessor of this vertex been established?
			if adjacent_vertex not in predecessor:
				q.put(adjacent_vertex) # Enqueue the vertex
				# Register which vertex comes next on a shortest path.
				predecessor[adjacent_vertex] = vertex

	return predecessor

# Find the path
def find_path(graph, s_vertex, e_vertex):
	"""
	Construct a list of vertices in order of the shortest path 
	from the s_vertex to the e_vertex in a graph.
	"""

	# Compute predecessor of each vertex on a shortest path
	# Call the bfs algorithm to build the predecessor dictionary
	predecessor = bfs(graph, s_vertex, e_vertex)

	# So the path is initially empty
	path = []

	if e_vertex in predecessor:
		# start at the end of the graph and move backwards
		path = [e_vertex]
		vertex = e_vertex

		while vertex != s_vertex:
			# Get vertex that comes before on the shortest path
			vertex = predecessor[vertex]
			# Prepend the predecessor vertex to the front of the path list
			path = [vertex] + path

	path = is_shortest_path(path)

	return path


# Check if no shortest path exist
def is_shortest_path(path):
	"""
	Checking if there is not shortest path in a particular 
	case.
	"""
	if not path:
		path = "No shortest path for this case."

	return path


# Checking if a graph is connected
def is_connected(G):
	"""
	G is the a dictionary that represents a graph. A graph containing no 
	vertices (and, therefore, no edges) is considered connected
	"""
	predecessor = bfs(G)
	for vertex in G:
		if vertex not in predecessor:
			return False
	return True


# Define the main routine
def main():
	"""
	Testing the find_path function by searching for routes between airports
	"""

	# Routes dictionary
	routes = {
				"ATL": {"MIA", "DCA", "ORD", "MCI", "DFW", "DEN"},
				"DFW": {"LAX", "DEN", "MCI", "ORD", "ATL", "MIA"},
				"LAX": {"SFO", "DEN", "MCI", "DFW"},
				"MIA": {"LGA", "DCA", "ATL", "DFW"},
				"DEN": {"SFO", "LAX", "MCI", "DFW", "SEA", "ATL"},
				"MCI": {"SFO", "DEN", "ORD", "LGA"},
				"DCA": {"DEN", "LAX", "DFW", "ATL", "ORD", "LGA"},
				"ORD": {"SEA", "MCI", "DFW", "ATL", "DCA", "LGA"},
				"LGA": {"SEA", "MCI", "ORD", "DCA", "MIA"},
				"SFO": {"SEA", "DEN", "LAX"},
				"CLT": {"BNA", "CHA"},
				"CHA": {"CLT", "BNA"},
				"SEA": {"SFO", "DEN", "ORD", "LGA"},
				"BNA": {"CLT", "CHA"},
			}

	G1 = {
			"ATL": {"MIA", "DCA", "ORD", "MCI", "DFW", "DEN"},
			"MIA": {"LGA", "DCA", "ATL", "DFW"},
			"DFW": {"LAX", "DEN", "MCI", "ORD", "ATL", "MIA"},
			"LAX": {"SFO", "DEN", "MCI", "DFW"},
			"DEN": {"SFO", "LAX", "MCI", "DFW", "SEA", "ATL"},
			"SEA": {"SFO", "DEN", "ORD", "LGA"},
			"MCI": {"DEN", "LAX", "DFW", "ATL", "ORD", "LGA"},
			"ORD": {"SEA", "MCI", "DFW", "ATL", "DCA", "LGA"},
			"DCA": {"ORD", "ATL", "MIA", "LGA"},
			"LGA": {"SEA", "MCI", "ORD", "DCA", "MIA"},
			"SFO": {"SEA", "DEN", "LAX"}
		}

	G2 = {
			"CLT": {"BNA", "CHA"},
			"BNA": {"CLT", "CHA"},
			"CHA": {"CLT", "BNA"}
		}

	# Attempt to find a route from one airport to another
	print(find_path(routes, "LAX", "DCA"))
	print(find_path(routes, "MIA", "SFO"))
	print(find_path(routes, "ATL", "MIA"))
	print(find_path(routes, "LGA", "DCA"))
	print(find_path(routes, "LAX", "ATL"))
	print(find_path(routes, "BNA", "ATL"))

	# Check if the graph is connected
	print("\n\nThis graph is connected: ", is_connected(routes))
	print("This graph is connected: ", is_connected(G1))
	print("This graph is connected: ", is_connected(G2))

if __name__ == "__main__":
	# call and execute the main routine
	main()